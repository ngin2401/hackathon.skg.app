package com.cui.hackathon_skg_app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cui.hackathon_skg_app.view.Fragment_List_Student;
import com.cui.hackathon_skg_app.R;
import com.cui.hackathon_skg_app.model.eAssign;
import com.cui.hackathon_skg_app.view.Fragment_Student_Submit;

import java.util.List;

public class Assign_Adapter extends ArrayAdapter<eAssign> implements View.OnClickListener {
    private List<eAssign> eAssignList;
    private int resource;
    private Context context;


    public Assign_Adapter(@NonNull Context context, int resource, @NonNull List<eAssign> objects) {
        super(context, resource, objects);
        this.context = context;
        this.eAssignList = objects;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(resource,parent,false);
        TextView textView = convertView.findViewById(R.id.baitap_textView);
        eAssign eAssign = eAssignList.get(position);
        textView.setText(eAssign.getTen_baitap());
        textView.setOnClickListener(this);
        return convertView;
    }

    @Override
    public void onClick(View v) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_layout,new Fragment_List_Student());
        fragmentTransaction.commit();
    }
}
