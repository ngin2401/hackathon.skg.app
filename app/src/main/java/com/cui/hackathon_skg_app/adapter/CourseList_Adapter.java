package com.cui.hackathon_skg_app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cui.hackathon_skg_app.view.Fragment_Assign;
import com.cui.hackathon_skg_app.R;
import com.cui.hackathon_skg_app.model.eCourse;
import com.cui.hackathon_skg_app.model.mCourse;

import java.util.List;

public class CourseList_Adapter extends RecyclerView.Adapter<CourseList_Adapter.ViewHolder> implements View.OnClickListener {
    private Context context;
    private List<eCourse> eCourseList;

    public CourseList_Adapter(Context context, List<eCourse> eCourseList) {
        this.context = context;
        this.eCourseList = eCourseList;
    }

    @NonNull
    @Override
    public CourseList_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_cardview,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CourseList_Adapter.ViewHolder holder, int position) {
        //eSourceList = new mSource().geteSourceList();
        eCourse eCourse = eCourseList.get(position);
        holder.mon.setText(eCourse.getTen_monhoc());
        holder.GV.setText(eCourse.getTen_gv());
        holder.mon.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return new mCourse().geteSourceList().size();
    }

    @Override
    public void onClick(View v) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_layout,new Fragment_Assign());
        fragmentTransaction.commit();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mon,GV;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mon = itemView.findViewById(R.id.mon_textView);
            GV = itemView.findViewById(R.id.gv_textView);
        }
    }
}
