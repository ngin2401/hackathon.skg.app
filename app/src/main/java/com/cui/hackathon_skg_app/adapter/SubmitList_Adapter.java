package com.cui.hackathon_skg_app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cui.hackathon_skg_app.view.Fragment_Grade;
import com.cui.hackathon_skg_app.R;
import com.cui.hackathon_skg_app.model.eStudent;

import java.util.List;

public class SubmitList_Adapter extends ArrayAdapter<eStudent> implements View.OnClickListener {

    private Context context;
    private int resource;
    private List<eStudent> studentList;

    public SubmitList_Adapter(@NonNull Context context, int resource, @NonNull List<eStudent> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.studentList = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(resource,parent,false);
        TextView textView = convertView.findViewById(R.id.baitap_textView);
        eStudent eStudent = studentList.get(position);
        textView.setText(eStudent.getName_Std());
        textView.setOnClickListener(this);
        return convertView;
    }

    @Override
    public void onClick(View v) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_layout,new Fragment_Grade());
        fragmentTransaction.commit();
    }
}
