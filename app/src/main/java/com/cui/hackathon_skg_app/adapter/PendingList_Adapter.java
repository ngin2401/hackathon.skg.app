package com.cui.hackathon_skg_app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cui.hackathon_skg_app.R;
import com.cui.hackathon_skg_app.model.eStudent;

import java.util.List;

public class PendingList_Adapter extends ArrayAdapter<eStudent> {
    private List<eStudent> eStudentList;
    private Context context;
    private int resource;

    public PendingList_Adapter(@NonNull Context context, int resource, @NonNull List<eStudent> objects) {
        super(context, resource, objects);
        this.eStudentList = objects;
        this.context = context;
        this.resource = resource;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(resource,parent,false);
        TextView textView = convertView.findViewById(R.id.baitap_textView);
        eStudent eStudent = eStudentList.get(position);
        textView.setText(eStudent.getName_Std());
        return convertView;
    }
}
