package com.cui.hackathon_skg_app.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.cui.hackathon_skg_app.view.Fragment_Pending_Tab;
import com.cui.hackathon_skg_app.view.Fragment_Submited_Tab;

public class SubmitAdapter extends FragmentStatePagerAdapter {
    private final String[]title_Tab = {"Pending","Submited"};

    public SubmitAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new Fragment_Pending_Tab();
                break;
            case 1:
                fragment = new Fragment_Submited_Tab();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return title_Tab[position];
    }
}
