package com.cui.hackathon_skg_app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.cui.hackathon_skg_app.view.AddCourseFragment;
import com.cui.hackathon_skg_app.view.AssignmentFragment;
import com.cui.hackathon_skg_app.view.CourseFragment;
import com.cui.hackathon_skg_app.view.Fragment_Course;
import com.cui.hackathon_skg_app.view.LogInFragment;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ActionBar actionBar;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private ActionBarDrawerToggle actionBarDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        //actionBarDrawerToggle = new ActionBarDrawerToggle(this,)
        Fragment fragment = new Fragment_Student_Submit();
        callFragment(fragment);

    }

    private void callFragment(Fragment fragment) {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        if(fragment.isAdded() == false){
            Log.d("Test Fragment","add");
            fragmentTransaction.remove(new LogInFragment());
            fragmentTransaction.replace(R.id.fragment_layout,fragment);
        }
        else {
            Log.d("Test Fragment","replace");
            fragmentTransaction.add(R.id.fragment_layout,fragment);
        }
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.about:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("About");
                builder.setMessage("Made by team Cùi");
                builder.setNegativeButton("OK",null);
                builder.create().show();
                return true;
            case R.id.exit:
                System.exit(0);
                return true;
            case R.id.add:
                Fragment fragment = new AddCourseFragment();
                callFragment(fragment);

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
<<<<<<< HEAD
        callFragment(new Fragment_Assign());
=======
        callFragment(new Fragment_Course());
>>>>>>> da sap xep + doi ten file
    }
}
