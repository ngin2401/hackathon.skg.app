package com.cui.hackathon_skg_app.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cui.hackathon_skg_app.R;
import com.cui.hackathon_skg_app.adapter.SubmitAdapter;

public class Fragment_List_Student extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private SubmitAdapter submitAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_student,container,false);
        tabLayout = view.findViewById(R.id.tab_layout);
        viewPager = view.findViewById(R.id.viewpager);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setViewPagerAdapter();
    }

    public void setViewPagerAdapter(){
        FragmentManager fragmentManager = getFragmentManager();
        submitAdapter = new SubmitAdapter(fragmentManager);
        viewPager.setAdapter(submitAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }
}
