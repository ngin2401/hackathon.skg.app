package com.cui.hackathon_skg_app.view;

import android.os.AsyncTask;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.cui.hackathon_skg_app.Fragment_Assign;
import com.cui.hackathon_skg_app.R;
import com.cui.hackathon_skg_app.model.User;
import com.cui.hackathon_skg_app.service.ApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;

public class LogInFragment extends Fragment {
    EditText usernameInput;
    EditText passwordInput;
    Button logInBtn;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment,container,false);
        usernameInput = view.findViewById(R.id.usernameInput);
        passwordInput = view.findViewById(R.id.pwdInput);
        logInBtn = view.findViewById(R.id.loginBtn);
        logInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                logIn(usernameInput.getText().toString(),passwordInput.getText().toString());
                RequestQueue queue = Volley.newRequestQueue(getContext());
                try {
                    final String url = "http://192.168.0.116:45455/api/Users/login";
                    final JSONObject jsonBody = new JSONObject("{\"username\":\"diep\",\"password\":\"123456\"}");
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            FragmentTransaction fragmentTransaction = ((FragmentActivity) getContext()).getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.fragment_layout,new Fragment_Course());
                            fragmentTransaction.commit();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("Login","error");

                        }
                    });
                    queue.add(request);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        return view;
    }
    public String parseJsonLoginString(String username, String password){
        return  "{'username':'" + username + "',"
                + "'password':'" + password + "'}";
    }
    public String parseJsonTokenString(String token, String id){
        return  "{'notiToken':'" + token + "',"
                + "'id':'" + id + "'}";
    }

    public void logIn(String username, String password)  {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        try {
            final String url = "http://192.168.0.116:45455/api/Users/login";
            final JSONObject jsonBody = new JSONObject(parseJsonLoginString(username,password));
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        JSONObject object = response.getJSONObject("result");
                        String token = FirebaseInstanceId.getInstance().getToken();
                        String id = object.getString("id");
                        sendTokenToServer(token,id);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Login","error");

                }

            });
            queue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendTokenToServer(String token, String id) {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        try {
            final String url = "http://192.168.0.116:45455/api/Users/AddNotiTokenFromUser";
            final JSONObject jsonBody = new JSONObject(parseJsonTokenString(token,id));
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject object = response.getJSONObject("result");
                        FragmentManager fragmentManager = ((FragmentActivity) getContext()).getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_layout,new Fragment_Assign());
                        fragmentTransaction.commit();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Login","api");

                }

            });
            queue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void sendData(){

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
