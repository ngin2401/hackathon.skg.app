package com.cui.hackathon_skg_app.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.cui.hackathon_skg_app.R;
import com.cui.hackathon_skg_app.adapter.PendingList_Adapter;
import com.cui.hackathon_skg_app.model.eStudent;
import com.cui.hackathon_skg_app.model.mStudent;

import java.util.List;

public class Fragment_Pending_Tab extends Fragment {
    private ListView listView;
    private PendingList_Adapter pendingList_adapter;
    private List<eStudent>eStudentList;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pending_tab,container,false);
        listView = view.findViewById(R.id.pending_list);
        eStudentList = new mStudent().geteStudentList();
        pendingList_adapter = new PendingList_Adapter(getActivity(),R.layout.row_listview,eStudentList);
        listView.setAdapter(pendingList_adapter);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
