package com.cui.hackathon_skg_app.view;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.cui.hackathon_skg_app.R;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;


public class AddCourseFragment extends Fragment {
    EditText name;
    EditText coursename;
    EditText description;
    Button post;

    public AddCourseFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_course,container,false);
        name = view.findViewById(R.id.Name);
        coursename = view.findViewById(R.id.CourseName);
        description = view.findViewById(R.id.Description);
        post = view.findViewById(R.id.post_btn);

        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postAssignment(name.getText().toString(),coursename.getText().toString(),description.getText().toString());
            }
        });

        return view;
    }
    public String parseJsonAssignmentString(String name, String courseName, String description){
        return  "{'name':'" + name + "'," +
                "'courseName':'" + courseName + "'," +
                "'description':'" + description + "'," +
                "'teacherId':'" + description + "'," +
                "'listStudent': '[\"2fc9e8b9-13f2-4225-b8e5-bdfce3f209d2\",\"dce3ac59-66c1-42eb-9053-02a477766e26\"]'," +
                "'startDate':'" + description + "'," +
                "'endDate':'" + description +
                "'}";
    }
    private void postAssignment(String name, String coursename, String description) {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        try {
            final String url = "http://192.168.0.116:45455/api/Assignment";
            final JSONObject jsonBody = new JSONObject(parseJsonAssignmentString(name,coursename,description));
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Login","suggest");
//                    try {
//                        JSONObject object = response.getJSONObject("result");
//                        String token = FirebaseInstanceId.getInstance().getToken();
//                        String id = object.getString("id");
////                        sendTokenToServer(token,id);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Login","error");

                }

            });
            queue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
