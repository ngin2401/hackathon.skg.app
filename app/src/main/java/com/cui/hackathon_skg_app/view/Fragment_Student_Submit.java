package com.cui.hackathon_skg_app.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.cui.hackathon_skg_app.R;

import java.io.File;

public class Fragment_Student_Submit extends Fragment{

    Button btnDone, btnView, btnSubmit;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_student_submit,container,false);

        btnView = view.findViewById(R.id.btnView);
        btnSubmit = view.findViewById(R.id.btnSubmit);
        btnDone = view.findViewById(R.id.btnDone);

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnView(view);
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSubmit(view);
            }
        });
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDone(view);
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void btnView(View view) {
        Intent intent2 = new Intent();
        intent2.setAction(Intent.ACTION_GET_CONTENT);
        intent2.setType("text/plain");
        startActivity(Intent.createChooser(intent2,"Select File"));
    }

    public void btnSubmit(View view) {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivity(Intent.createChooser(intent,"Select File"));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedfile = data.getData();
        Toast.makeText(getActivity(),selectedfile.getPath(),Toast.LENGTH_SHORT);
    }

    public void btnDone(View view) {
        Log.d("Student: Done", "button clicked");
    }
}
