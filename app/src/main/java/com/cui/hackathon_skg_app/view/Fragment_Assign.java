package com.cui.hackathon_skg_app.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.cui.hackathon_skg_app.R;
import com.cui.hackathon_skg_app.adapter.Assign_Adapter;
import com.cui.hackathon_skg_app.model.eAssign;
import com.cui.hackathon_skg_app.model.mAssign;

import java.util.List;

public class Fragment_Assign extends Fragment {

    private ListView listView;
    private com.cui.hackathon_skg_app.adapter.Assign_Adapter Assign_Adapter;
    private List<eAssign> eAssignList;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_assign,container,false);
        listView = view.findViewById(R.id.listView);
        eAssignList = new mAssign().geteAssignList();
        Assign_Adapter = new Assign_Adapter(getActivity(),R.layout.row_listview,eAssignList);
        listView.setAdapter(Assign_Adapter);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
