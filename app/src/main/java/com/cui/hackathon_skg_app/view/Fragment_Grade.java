package com.cui.hackathon_skg_app.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cui.hackathon_skg_app.R;

public class Fragment_Grade extends Fragment {

    static int grade;
    TextView gradeTV;
    SeekBar seekBar;
    Button btnView, btnPlus, btnMinus, btnDone;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grade,container,false);

        // Init
        gradeTV = view.findViewById(R.id.gradeTV);
        seekBar = view.findViewById(R.id.sb_grade);

        btnView = view.findViewById(R.id.btn_view_assignment);
        btnPlus = view.findViewById(R.id.btn_grade_plus);
        btnMinus = view.findViewById(R.id.btn_grade_minus);
        btnDone = view.findViewById(R.id.btn_done);

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnViewFile(view);
            }
        });

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnGradePlus(view);
            }
        });
        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnGradeMinus(view);
            }
        });
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDone(view);
            }
        });


        // perform seek bar change listener event used for getting the progress value
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 5;


            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                gradeTV.setText(String.valueOf(progress));
                progressChangedValue = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d("Grade: Grade", progressChangedValue + "");
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    public void btnViewFile(View view) {
        Log.d("Grade: View", "button clicked");
    }

    public void btnGradeMinus(View view) {
        Log.d("Grade: Minus", "button clicked");
        grade = seekBar.getProgress();
        if (grade > 0) {
            grade--;
        }

        seekBar.setProgress(grade);
        gradeTV.setText(String.valueOf(grade));
    }

    public void btnGradePlus(View view) {
        Log.d("Grade: Plus", "button clicked");
        grade = seekBar.getProgress();
        if (grade < 10) {
            grade++;
        }
        seekBar.setProgress(grade);
        gradeTV.setText(String.valueOf(grade));
    }

    public void btnDone(View view) {
        Toast.makeText(getActivity(),"Giáo viên đả chấm "+grade+" điểm",Toast.LENGTH_SHORT).show();
        grade = 0;
    }
}
