package com.cui.hackathon_skg_app.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cui.hackathon_skg_app.R;
import com.cui.hackathon_skg_app.adapter.CourseList_Adapter;
import com.cui.hackathon_skg_app.model.eCourse;
import com.cui.hackathon_skg_app.model.mCourse;

import java.util.List;

public class Fragment_Course extends Fragment {
    private com.cui.hackathon_skg_app.adapter.CourseList_Adapter CourseList_Adapter;
    private List<eCourse> eCourseList;
    private RecyclerView recyclerView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_course,container,false);
        recyclerView = view.findViewById(R.id.recycleView);
        eCourseList = new mCourse().geteSourceList();

        CourseList_Adapter = new CourseList_Adapter(getActivity(),eCourseList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(CourseList_Adapter);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

}
