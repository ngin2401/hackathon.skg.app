package com.cui.hackathon_skg_app.model;

import java.util.ArrayList;
import java.util.List;

public class mSource {
    private List<eCourse> eSourceList;

    public List<eCourse> geteSourceList() {
        eCourse eSource1 = new eCourse("mon hoc 1", "giao vien 1");
        eCourse eSource2 = new eCourse("mon hoc 2", "giao vien 2");
        eCourse eSource3 = new eCourse("mon hoc 3", "giao vien 3");
        eCourse eSource4 = new eCourse("mon hoc 4", "giao vien 4");

        eSourceList = new ArrayList<>();
        eSourceList.add(eSource1);
        eSourceList.add(eSource2);
        eSourceList.add(eSource3);
        eSourceList.add(eSource4);
        return eSourceList;
    }
}
