package com.cui.hackathon_skg_app.model;

import java.util.ArrayList;
import java.util.List;

public class mCourse{
    private List<eCourse> eCourseList;

    public List<eCourse> geteSourceList() {
        eCourse eSource1 = new eCourse("mon hoc 1", "giao vien 1");
        eCourse eSource2 = new eCourse("mon hoc 2", "giao vien 2");
        eCourse eSource3 = new eCourse("mon hoc 3", "giao vien 3");
        eCourse eSource4 = new eCourse("mon hoc 4", "giao vien 4");

        eCourseList = new ArrayList<>();
        eCourseList.add(eSource1);
        eCourseList.add(eSource2);
        eCourseList.add(eSource3);
        eCourseList.add(eSource4);
        return eCourseList;
    }
}
