package com.cui.hackathon_skg_app.model;

public class eCourse {
    private String ten_monhoc;
    private String ten_gv;

    public eCourse(String ten_monhoc, String ten_gv) {
        this.ten_monhoc = ten_monhoc;
        this.ten_gv = ten_gv;
    }

    public String getTen_monhoc() {
        return ten_monhoc;
    }

    public void setTen_monhoc(String ten_monhoc) {
        this.ten_monhoc = ten_monhoc;
    }

    public String getTen_gv() {
        return ten_gv;
    }

    public void setTen_gv(String ten_gv) {
        this.ten_gv = ten_gv;
    }
}
