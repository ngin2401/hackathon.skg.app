package com.cui.hackathon_skg_app.model;

import java.util.ArrayList;

public class User {
    String username;
    String password;
    String name;
    String role;
    ArrayList<String> list;
    String id;
    String addedAtUtc;


    public User(){

    }

    public User(String username, String password, String name, String role, ArrayList<String> list, String id, String addedAtUtc) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.role = role;
        this.list = list;
        this.id = id;
        this.addedAtUtc = addedAtUtc;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public ArrayList<String> getList() {
        return list;
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddedAtUtc() {
        return addedAtUtc;
    }

    public void setAddedAtUtc(String addedAtUtc) {
        this.addedAtUtc = addedAtUtc;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
