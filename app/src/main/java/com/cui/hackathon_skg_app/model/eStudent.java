package com.cui.hackathon_skg_app.model;

public class eStudent {
    private String name_Std;

    public eStudent(String name_Std) {
        this.name_Std = name_Std;
    }

    public String getName_Std() {
        return name_Std;
    }

    public void setName_Std(String name_Std) {
        this.name_Std = name_Std;
    }
}
