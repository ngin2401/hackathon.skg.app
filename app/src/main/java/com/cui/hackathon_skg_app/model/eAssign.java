package com.cui.hackathon_skg_app.model;

import java.util.ArrayList;

public class eAssign {
    private String ten_baitap;
    String courseName;
    String description;
    String teacherID;
    ArrayList<String> students;
    String startDate;
    String endDate;
    String id;

    public eAssign(){

    }
    public eAssign(String ten_baitap, String courseName, String description, String teacherID, ArrayList<String> students, String startDate, String endDate, String id) {
        this.ten_baitap = ten_baitap;
        this.courseName = courseName;
        this.description = description;
        this.teacherID = teacherID;
        this.students = students;
        this.startDate = startDate;
        this.endDate = endDate;
        this.id = id;
    }

    public String getTen_baitap() {
        return ten_baitap;
    }

    public void setTen_baitap(String ten_baitap) {
        this.ten_baitap = ten_baitap;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    public ArrayList<String> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<String> students) {
        this.students = students;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
