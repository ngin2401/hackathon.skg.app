package com.cui.hackathon_skg_app.model;

import java.util.ArrayList;
import java.util.List;

public class mStudent {
    private List<eStudent> eStudentList;
    private eStudent[] eStudents;

    public List<eStudent> geteStudentList() {
        eStudentList = new ArrayList<>();
        eStudents = new eStudent[10];
        for(int i=0; i<eStudents.length;i++){
            eStudents[i] = new eStudent("Nguyen Van "+ i);
            eStudentList.add(eStudents[i]);
        }
        return eStudentList;
    }
}
